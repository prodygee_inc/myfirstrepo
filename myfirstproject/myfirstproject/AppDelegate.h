//
//  AppDelegate.h
//  myfirstproject
//
//  Created by user113087 on 9/27/15.
//  Copyright © 2015 Prodygee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

